# fed-e-task-02-02

### 1、Webpack 的构建流程主要有哪些环节？如果可以请尽可能详尽的描述 Webpack 打包的整个过程。
- 识别入口文件，entry属性
- 通过逐层识别模块依赖：commonjs、ESModule、AMD等等，来获取代码的依赖项，最后形成一个依赖树
- 通过分析代码（AST），转换代码（Loader），编译代码，输出代码（Plugin）最终形成打包后的代码


### 2、Loader 和 Plugin 有哪些不同？请描述一下开发 Loader 和 Plugin 的思路。

- Loader是一个文件转换器，它是转换指定类型的文件成为另一类文件的，单纯的文件转换。

- Plugin是一个扩展器，可以用于打包的整个过程，不直接操作文件，而是基于事件机制，监听webpack打包过程中的某些节点，来执行一些对应的任务

- 开发Loader：

    - 需要导出一个函数，输入是需要转换的文件内容，输出的就是一个转换后的内容（通过JavaScript代码包装后输出），类似一个工作管道
    - 转换的过程是可以经过多个loader处理的，最终形成需要的内容
    ``` javascript
    const marked = require('marked')
    
    module.exports = source => {
        const html = marked(source)
        return `module.exports = ${JSON.stringify(html)}`
    }
    ```

- 开发Plugin：

    - Plugin通过hooks机制来实现

    - Plugin必须是一个函数或者是一个包含apply方法的对象

    - 在开发Plugin时一般是通过定义一个类class，然后通过创建该类的实例来使用

    ``` javascript
    // 清除bundle.js前面的注释
    class MyPlugin {
        apply(compiler) {
            compiler.hooks.emit.tap('MyPlugin', compilation => {
                for (const name in compilation.assets) {
                    
                    if (name.endsWith('.js')) {
                        const contents = compilation.assets[name].source()
                        const withoutComments = contents.replace(/\/\*\*+\*\//g, '')
                        compilation.assets[name] = {
                            source: () => withoutComments,
                            size: () => withoutComments.length
                        }
                    }
                }
            })
        }
    }
    ```