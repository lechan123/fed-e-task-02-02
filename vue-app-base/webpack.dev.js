const common = require('./webpack.common')

module.exports = Object.assign({}, common, {
    mode: 'development',
    devtool: 'cheap-module-eval-source-map',
    devServer: {
        contentBase: ['./public']
    }
})