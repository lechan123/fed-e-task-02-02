const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const StyleLintPlugin = require('stylelint-webpack-plugin')
const webpack = require('webpack')

module.exports = {
    entry: './src/main.js',
    output: {
        filename: '[name]-[contenthash:8].bundle.js',
        path: path.resolve(__dirname, './dist')
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    module: {
        rules: [
            {
                test: /.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /.js$/,
                loader: 'babel-loader',
                exclude: file => (
                    /node_modules/.test(file) &&
                    !/\.vue\.js/.test(file)
                )
            },
            {
                test: /.css$/,
                use: [
                    'style-loader', // 使用vue-style-loader不能生成style标签
                    'css-loader',
                    'postcss-loader'
                ]
            },
            {
                test: /.less$/,
                use: [
                    'style-loader', // 使用vue-style-loader不能生成style标签
                    'css-loader',
                    'less-loader'
                ]
            },
            {
                test: /.(png|jpe?g|gif)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        esModule: false,
                        name: '[name].[ext]',
                        limit: 10 * 1024
                    }
                }]
            },
            {
                enforce: 'pre',
                test: /\.(js|vue)$/,
                loader: 'eslint-loader',
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            title: 'vue-app-base',
            url: '',
            template: 'public/index.html'
        }),
        new StyleLintPlugin({
            files: ['**/*.{vue,htm,html,css,sss,less,scss,sass}'],
        }),
        new webpack.DefinePlugin({
            API_BASE_URL: JSON.stringify('http://api.example.com')
        })
    ]
}